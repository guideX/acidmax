Attribute VB_Name = "mdlSubs"
Option Explicit

Public Sub PlayFile()
On Local Error Resume Next
Dim msg As String
msg = OpenDialog(frmMain, "VOB Files (*.vob)|*.vob|All Files (*.*)|*.*", "nexDVD", App.Path)
If Len(msg) <> 0 Then
    frmMain.VOB.Open msg
    frmMain.VOB.Play
End If
End Sub
