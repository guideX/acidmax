VERSION 5.00
Object = "{48E59290-9880-11CF-9754-00AA00C00908}#1.0#0"; "MSINET.OCX"
Begin VB.Form frmMain 
   Caption         =   "Inet Tetris Upload"
   ClientHeight    =   2175
   ClientLeft      =   0
   ClientTop       =   240
   ClientWidth     =   6150
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2175
   ScaleWidth      =   6150
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   975
      Left            =   2040
      TabIndex        =   1
      Top             =   720
      Width           =   1575
   End
   Begin VB.Timer tmrEnd 
      Enabled         =   0   'False
      Interval        =   50000
      Left            =   5640
      Top             =   1200
   End
   Begin VB.ListBox lstWaiting 
      Appearance      =   0  'Flat
      Height          =   2175
      IntegralHeight  =   0   'False
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5415
   End
   Begin VB.Timer tmrCheckCaption 
      Interval        =   500
      Left            =   5640
      Top             =   720
   End
   Begin InetCtlsObjects.Inet ctlUpload 
      Left            =   5520
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const GW_HWNDPREV = 3
Private Declare Function OpenIcon Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function GetWindow Lib "user32" (ByVal hWnd As Long, ByVal wCmd As Long) As Long
Private Declare Function SetForegroundWindow Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Declare Function GetPrivateProfileString Lib "Kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString& Lib "Kernel32" Alias "WritePrivateProfileStringA" (ByVal AppName$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)
Dim lConnected As Boolean, lLoggedIn As Boolean, lNickname As String, lBusy As Boolean, lCaption As String

Public Function ReadINI(ByVal lFile As String, ByVal Section As String, ByVal Key As String, Optional lDefault As String)
On Local Error Resume Next
Dim msg As String, RetVal As String, Worked As Integer
RetVal = String$(255, 0)
Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), lFile)
If Worked = 0 Then
    ReadINI = lDefault
Else
    ReadINI = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
End If
End Function

Public Sub WriteINI(ByVal lFile As String, ByVal Section As String, ByVal Key As String, ByVal Value As String)
On Local Error Resume Next
WritePrivateProfileString Section, Key, Value, lFile
End Sub

Public Function DisconnectFromTetrisNetwork(lInet As Inet) As Boolean
On Local Error GoTo hErr
With lInet
    .Cancel
    DoEvents
    .Execute , "CLOSE"
    lConnected = False
    Do While .StillExecuting = True
        DoEvents
    Loop
End With
DisconnectFromTetrisNetwork = False
lstWaiting.AddItem "Connection to Tetris Network closed"
Exit Function
hErr:
    lstWaiting.AddItem "Disconnect Error: " & Err.Description
End Function

Public Function UploadFile(lLocalFile As String, lRemoteFile As String, lInet As Inet) As Boolean
On Local Error GoTo hErr
With lInet
    .Execute , "PUT " & Chr(34) & lLocalFile & Chr(34) & " " & Chr(34) & lRemoteFile & Chr(34)
    Do While .StillExecuting = True
        DoEvents
    Loop
    lstWaiting.AddItem "File Uploaded"
    .Execute , "DIR "
    Do While .StillExecuting = True
        DoEvents
    Loop
End With
UploadFile = True
Shell App.Path & "\LoadTetrisUpload.exe", vbNormalFocus
End
Exit Function
hErr:
    lstWaiting.AddItem "Error sending file: " & Err.Description, vbExclamation
End Function

Public Function RemoveFile(lFileName As String, lInet As Inet) As Boolean
On Local Error GoTo hErr
With lInet
    .Execute , "DELETE " & Chr(34) & lFileName & Chr(34)
    Do While .StillExecuting = True
        DoEvents
    Loop
    lstWaiting.AddItem "File Deleted"
End With
RemoveFile = True
Exit Function
hErr:
End Function

Public Function DownloadFile(lUrl As String, lLocalFile As String, lInet As Inet) As Boolean
On Local Error GoTo hErr
Dim lData() As Byte
With lInet
    lData() = .OpenURL(lUrl, icByteArray)
    Open lLocalFile For Binary Access Write As #1
        Put #1, , lData()
    Close #1
    Do While .StillExecuting = True
        DoEvents
    Loop
    lstWaiting.AddItem "File Downloaded"
End With
Exit Function
hErr:
    lstWaiting.AddItem "Error downloading file: " & lUrl
End Function

Public Function CheckNickname() As Boolean
On Local Error Resume Next
lNickname = GetSetting("Tetris", "Settings", "Nickname", "")
If Len(lNickname) = 0 Then
    End
'    lNickname = InputBox("Enter your multiplayer nickname:", App.Title, "")
    'If Len(lNickname) = 0 Then
'    End
'    Exit Function
    'Else
    '    CheckNickname = True
    'End If
Else
    CheckNickname = True
End If
End Function

Public Function EnterDirectory(lInet As Inet, lDirectory As String) As Boolean
On Local Error GoTo hErr
With lInet
    .Execute , "CD " & Chr(34) & lDirectory & Chr(34): DoEvents
End With
lstWaiting.AddItem "Directory Entered"
EnterDirectory = True
Exit Function
hErr:
    lInet.Cancel
    lstWaiting.AddItem "Error occured entering directory: " & Err.Description, vbExclamation
End Function

Public Function CreateDirectory(lInet As Inet, lDirectory As String) As Boolean
On Local Error GoTo hErr
With lInet
    .Execute , "MKDIR " & Chr(34) & lDirectory & Chr(34): DoEvents
End With
CreateDirectory = True
lstWaiting.AddItem "Directory Created"
Exit Function
hErr:
    lInet.Cancel
    lstWaiting.AddItem "Error occured creating directory: " & Err.Description, vbExclamation
End Function

Public Function LoginAndUploadFile(lServer As String, lUsername As String, lPassword As String, lRemoteFile As String, lLocalFile As String, lNick As String, lInet As Inet) As Boolean
On Local Error GoTo hErr
Dim i As Integer
With lInet
    .Protocol = icFTP
    .AccessType = icUseDefault
    .RequestTimeout = 40
    .URL = lServer
    .UserName = lUsername
    .Password = lPassword
    .Execute , "CD " & Chr(34) & "tetris" & Chr(34): DoEvents
    Do While .StillExecuting
        DoEvents
    Loop
    .Execute , "PUT " & Chr(34) & lLocalFile & Chr(34) & " " & Chr(34) & lRemoteFile & Chr(34)
    Do While .StillExecuting = True
        DoEvents
    Loop
    i = Int(ReadINI(App.Path & "\" & lNickname & ".ini", "Settings", "Count", 0))
    i = i + 1
    WriteINI App.Path & "\" & lNick & ".ini", "Settings", "Count", Trim(Str(i))
    WriteINI App.Path & "\" & lNick & ".ini", Trim(Str(i)), "Filename", lRemoteFile
    DoEvents
    .Execute , "PUT " & Chr(34) & App.Path & "\" & lNick & ".ini" & Chr(34) & " " & Chr(34) & lNick & ".ini" & Chr(34)
    Do While .StillExecuting = True
        DoEvents
    Loop
End With
'Shell App.Path & "\ltu.exe", vbNormalFocus
End
Exit Function

hErr:
    lstWaiting.AddItem "Error occured on login or uploading: " & Err.Description
    'msgbox "Error occured on login or uploading: " & Err.Description
End Function

Public Function CleanupServer(mNickname As String, lInet As Inet) As Boolean
On Local Error GoTo hErr
Dim i As Integer
With lInet
    .Execute , "CD " & Chr(34) & "tetris" & Chr(34): DoEvents
    Do While .StillExecuting = True
        DoEvents
    Loop
    '.Execute , "DIR"
    'For i = lStartingImageIndex To lEndingImageIndex
    '    .Execute , "DELETE " & Chr(34) & Left(lFileName, Len(lFileName) - 4) & Trim(Str(i)) & ".jpg" & Chr(34): DoEvents
    '    Do While .StillExecuting = True
    '        DoEvents
    '    Loop
    'Next i
    CleanupServer = True
End With
Exit Function
hErr:
    lstWaiting.AddItem "Error occured deleting file"
End Function

Public Function ConnectToServer(lServer As String, lUsername As String, lPassword As String, lInet As Inet) As Boolean
On Local Error GoTo hErr
With lInet
    .Protocol = icFTP
    .AccessType = icUseDefault
    .RequestTimeout = 40
    .URL = lServer
    .UserName = lUsername
    .Password = lPassword
'    .Execute , "CD " & Chr(34) & "public_html" & Chr(34): DoEvents
'    Do While .StillExecuting
'        DoEvents
'    Loop
    .Execute , "CD " & Chr(34) & "tetris" & Chr(34): DoEvents
    Do While .StillExecuting
        DoEvents
    Loop
    'Caption = "Inet Tetris Upload"
    Caption = lCaption
End With
lLoggedIn = True
ConnectToServer = True
lstWaiting.AddItem "Connected to Tetris Server"
Exit Function
hErr:
    lstWaiting.AddItem "Error occured connecting: " & Err.Description, vbExclamation
    lInet.Cancel
    ConnectToServer = False
End Function

Public Function CreateServerList(lServer As String, lUsername As String, lPassword As String, lInet As Inet) As Boolean
On Local Error GoTo hErr
With lInet
    .Protocol = icFTP
    .AccessType = icUseDefault
    .RequestTimeout = 40
    .URL = lServer
    .UserName = lUsername
    .Password = lPassword
    .Execute , "CD " & Chr(34) & "games" & Chr(34): DoEvents
    Do While .StillExecuting
        DoEvents
    Loop
    .Execute , "DIR " & Chr(34) & "games" & Chr(34): DoEvents
    Do While .StillExecuting
        DoEvents
    Loop
    Do While .StillExecuting = True
        DoEvents
    Loop
End With
Shell App.Path & "\ltu.exe", vbNormalFocus
End
Exit Function
hErr:
    lstWaiting.AddItem "Error occured on login or uploading: " & Err.Description
    'msgbox "Error occured on login or uploading: " & Err.Description
End Function

Public Sub ProcessCommand(lCommand As String)
On Local Error Resume Next
Dim l As Long, a() As String
frmMain.lstWaiting.BackColor = vbGreen
l = CLng(Left(lCommand, 3))
lCommand = Right(lCommand, Len(lCommand) - 3)
frmMain.tmrCheckCaption.Enabled = False
lBusy = True
Select Case l
Case 100
    a() = Split(lCommand, "*")
    If ConnectToServer(a(0), a(1), a(2), ctlUpload) = False Then
        lstWaiting.AddItem "Connection wasn't established"
        lstWaiting.BackColor = vbRed
        lBusy = False
        'Caption = "Inet Tetris Upload"
        Caption = lCaption
        frmMain.tmrCheckCaption.Enabled = True
        Exit Sub
    End If
Case 422
    a() = Split(lCommand, "*")
    If CreateServerList(a(0), a(1), a(2), ctlUpload) = False Then
        lstWaiting.AddItem "List wasn't created"
        lstWaiting.BackColor = vbRed
        lBusy = False
        'Caption = "Inet Tetris Upload"
        Caption = lCaption
        frmMain.tmrCheckCaption.Enabled = True
        Exit Sub
    End If
Case 421
    a() = Split(lCommand, "*")
    If CleanupServer(a(0), Int(a(1)), Int(a(2)), ctlUpload) Then
        lstWaiting.AddItem "Error Cleaning up Server"
        lstWaiting.BackColor = vbRed
        lBusy = False
        'Caption = "Inet Tetris Upload"
        Caption = lCaption
        frmMain.tmrCheckCaption.Enabled = True
    End If
Case 420
    a() = Split(lCommand, "*")
    If LoginAndUploadFile(a(0), a(1), a(2), a(3), a(4), a(5), ctlUpload) = False Then
        lstWaiting.AddItem "Error During Login or Upload"
        lstWaiting.BackColor = vbRed
        lBusy = False
        'Caption = "Inet Tetris Upload"
        Caption = lCaption
        frmMain.tmrCheckCaption.Enabled = True
        Exit Sub
    End If
Case 200
    If RemoveFile(lCommand, ctlUpload) = False Then
        lstWaiting.AddItem "File could not be removed"
        lstWaiting.BackColor = vbRed
        lBusy = False
        'Caption = "Inet Tetris Upload"
        Caption = lCaption
        frmMain.tmrCheckCaption.Enabled = True
        Exit Sub
    End If
Case 300
    If EnterDirectory(ctlUpload, lCommand) = False Then
        lstWaiting.AddItem "Directory could not be entered"
        lstWaiting.BackColor = vbRed
        lBusy = False
        Caption = lCaption
        'Caption = "Inet Tetris Upload"
        frmMain.tmrCheckCaption.Enabled = True
        Exit Sub
    End If
Case 500
    a() = Split(lCommand, "*")
    If UploadFile(a(0), a(1), ctlUpload) = False Then
        lstWaiting.AddItem "File could not be uploaded"
        lstWaiting.BackColor = vbRed
        lBusy = False
        Caption = lCaption
'        Caption = "Inet Tetris Upload"
        frmMain.tmrCheckCaption.Enabled = True
        Exit Sub
    End If
Case 600
    a() = Split(lCommand, "*")
    If DownloadFile(a(0), a(1), ctlUpload) = False Then
        lstWaiting.AddItem "File could not be downloaded"
        lstWaiting.BackColor = vbRed
        lBusy = False
        Caption = lCaption
'        Caption = "Inet Tetris Upload"
        frmMain.tmrCheckCaption.Enabled = True
        Exit Sub
    End If
Case 999
    CleanupServer
    Unload Me
    End
End Select
lBusy = False
frmMain.tmrCheckCaption.Enabled = True
frmMain.lstWaiting.BackColor = vbWhite
'Caption = "Inet Tetris Upload"
Caption = lCaption
End Sub


Public Sub ActivatePrevInstance()
On Local Error Resume Next
Dim OldTitle As String, PrevHndl As Long, result As Long
OldTitle = App.Title
App.Title = "BAD INSTANCE"
PrevHndl = FindWindow("ThunderRT6Main", OldTitle)
If PrevHndl = 0 Then
    Exit Sub
End If
PrevHndl = GetWindow(PrevHndl, GW_HWNDPREV)
result = OpenIcon(PrevHndl)
result = SetForegroundWindow(PrevHndl)
End
End Sub

Private Sub Command1_Click()
Caption = "999"
End Sub

Private Sub ctlUpload_StateChanged(ByVal State As Integer)
On Local Error Resume Next
Select Case State
Case icResponseCompleted
    Dim mbool As Boolean, msg As String, mFiles As String
    mbool = False
    Do While mbool = False
        msg = ctlUpload.GetChunk(4096, icString)
        If Len(msg) = 0 Then
            mbool = True
            Exit Do
        End If
        DoEvents
        mFiles = mFiles & msg
    Loop
'    'msgbox mFiles
End Select
End Sub

Private Sub Form_Load()
On Local Error Resume Next
Dim i As Integer
''msgbox Command$
'If Len(lCaption) = 0 Then
i = Int(Command$)
If i <> 0 Then
    lCaption = "Inet Tetris Upload " & Trim(Str(i))
Else
    lCaption = "Inet Tetris Upload"
End If
Me.Caption = lCaption
CheckNickname
'If App.PrevInstance = True Then
'    ActivatePrevInstance
'    Unload Me
'    Exit Sub
'End If
tmrEnd.Enabled = True
End Sub

Private Sub Form_Resize()
On Local Error Resume Next
lstWaiting.Width = Me.ScaleWidth
lstWaiting.Height = Me.ScaleHeight
End Sub

Private Sub tmrCheckCaption_Timer()
On Local Error Resume Next
Select Case Left(Me.Caption, 18)
Case "Inet Tetris Upload"
    Exit Sub
Case Else
    If lBusy = False Then
        ProcessCommand Me.Caption
    Else
        lstWaiting.AddItem Me.Caption
    End If
End Select
End Sub

Private Sub tmrEnd_Timer()
On Local Error Resume Next
End
End Sub
