VERSION 5.00
Begin VB.Form frmLoadTetrisUpload 
   Caption         =   "Load Tetris Upload"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4680
   Icon            =   "frmLoadTetrisUpload.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   WindowState     =   1  'Minimized
   Begin VB.Timer tmrLoad 
      Enabled         =   0   'False
      Interval        =   700
      Left            =   840
      Top             =   1320
   End
End
Attribute VB_Name = "frmLoadTetrisUpload"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
tmrLoad.Enabled = True
End Sub

Private Sub tmrLoad_Timer()
'On Local Error Resume Next
tmrLoad.Enabled = False
Shell App.Path & "\Tetris Upload.exe", vbNormalFocus
End
End Sub
